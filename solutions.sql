-- a.

SELECT * FROM artists WHERE name LIKE "%d%" OR "%d" OR "d%" OR "D%"

-- b.

SELECT * FROM songs WHERE length < 230;

-- c.

SELECT albums.album_title, songs.song_title, songs.length
	FROM albums JOIN songs ON albums.id = songs.album_id;

-- d. 

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "A%";

-- e.

SELECT * FROM albums ORDER BY song_name DESC;

-- f.

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
    ORDER BY SONG_title ASC
	